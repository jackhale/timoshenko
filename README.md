# Timoshenko beam finite element solver

This program implements a finite element solver for the Timoshenko beam theory
using the DOLFIN finite element problem solving environment from the [FEniCS
project](http://www.fenicsproject.org). The solver implements the well-known
continuous-displacement/constant-shear mixed finite element formulation which
eliminates the shear-locking problem. This solver was created as part of my
[PhD](https://orbilu.uni.lu/handle/10993/12057) thesis at Imperial College
London supervised by Pedro M. Baiz. You can download it
[here](https://orbilu.uni.lu/handle/10993/12057) under a Creative Commons-type
license.

## Author

Jack S. Hale

## Installation

Download and install FEniCS/DOLFIN for your platform following the instructions
at http://fenicsproject.org. Then either download the latest version of this
package from the 'Downloads' folder above or use git to clone:

    git clone git@bitbucket.org:jackhale/timoshenko.git

## Running

Change directory into the folder:

    cd timoshenko

Run the problem varying the thickness and keeping the mesh size and
the degree of finite element space the same:

    python cantilever.py

You can run your own experiments by passing the thickness parameter,
degree of the finite element space, and the number of elements to
the function `main`, e.g.:

    main(nx=50, degree=2, epsilon=1E-4)

## License

timoshenko is free software: you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

timoshenko is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with cosserat. If not, see <http://www.gnu.org/licenses/>.

## Citing

If you use this solver in your work, please cite my
[PhD](https://orbilu.uni.lu/handle/10993/12057) as well as the FEniCS project:
[FEniCS project citation instructions](http://fenicsproject.org/citing/)

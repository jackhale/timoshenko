from dolfin import *

l2 = lambda u,v: inner(u,v)*dx
h1semi = lambda u,v: inner(grad(u),grad(v))*dx
h1 = lambda u,v: l2(u,v) + h1semi(u,v)

def error_norm(u_exact, u_h, V_e, norm=l2):
    """ Given an exact solution u_exact (df.Expression)
    and an approximate solution u_h (df.Function), interpolate both into the
    finite element space V_e, and calculate the error in the norm
    specified
    
    returns: double
    """
    # Interpolate the approximate and exact solution into a higher
    # order FE space
    u_h_Ve = interpolate(u_h, V_e)
    u_e_Ve = interpolate(u_exact, V_e)
    e_Ve = Function(V_e)

    # Subtract approximate solution vector from exact to
    # form error solution vector
    e_Ve.vector()[:] = u_e_Ve.vector().array() - u_h_Ve.vector().array()

    # Assemble and return square-root of error
    error = sqrt(assemble(norm(e_Ve, e_Ve)))
    return error

def relative_error(u_exact, u_h, V_e, norm=l2):
    """ Given an exact solution u_exact (df.Expression object) and
    an approximate solution u_h (df.Function), interpolate both into the finite element
    space V_e and calculate the relative error ||u - u_exact||/||u_exact||
    in the norm specified
    
    returns: double
    """
    # Calculate distance between exact and approximate solution in norm
    error = error_norm(u_exact, u_h, V_e, norm=norm)

    # Calculate magnitude of exact solution in norm
    u_e_Ve = interpolate(u_exact, V_e)
    total = sqrt(assemble(norm(u_e_Ve, u_e_Ve)))

    relative = error/total

    return relative
